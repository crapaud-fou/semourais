---
Title: Fabriquer sa terre
Weight: 2
Img: img/terre.jpg
---
Pendant des siècles, les hommes ont pensé le monde composé de quatre éléments: la terre, l'air, l'eau et le feu.  
Nous avons dompté le plus redoutable d'entre eux pour en tirer chaleur et lumière. Nous avons emprisonné le feu dans des métaux et des machines qui ont changé la face du monde et fait reculer la nature.  
Le premier élément, la terre, le nom que nous avons pourtant donné à notre planète, reste un continent  mystérieux. Nous découvrons à peine la richesse de la vie qu'elle porte.  
Qui sait que la biodiversité d'un écosystème repose sur la biodiversité de son sol, de sa terre ?  
En compostant avec des techniques variées, de déchets on peut tirer une terre fertile, pour tous les êtres vivants...  