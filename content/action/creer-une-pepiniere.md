---
Title: Créer une pépinière
Weight: 3
img: img/pepiniere.jpg
---
Planter des forêts denses avec deux ou trois plants par mètre carré et des dizaines d'espèces, nécessite beaucoup d'arbres. C'est pourquoi nous avons décidé de créer des pépinières, d'abord avec des experts, maintenant avec des écoles et des particuliers. Ces pépinières d'arbres et d'arbustes alimenteront nos forêts de demain.  
Créer une pépinière, c'est à la portée de tous.  
Et, comme nous allons vous le montrer, on peut le faire avec des matériaux recyclés, donc sans argent...