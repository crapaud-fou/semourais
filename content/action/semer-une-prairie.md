---
Title: Semer une prairie
Weight: 4
Img: img/prairie.jpg
---
Trouver un terrain à même d'accueillir une forêt, même une petite, n'est pas chose facile.  
Il faut être sûr qu'elle sera préservée pour des dizaines d'années.  
Mais, de la biodiversité peut être restaurée rapidement en semant une prairie fleurie.  
C'est la première étape dans la nature vers le retour d'une forêt. Elle prépare le sol à des plantes plus grandes et plus robustes. Elle ameublit la terre aussi.  
Ses fleurs qui nous émerveillent, nourrissent les abeilles et les pollinisateurs aux populations en danger.  
Et puis, semer une prairie peut être le début d'une grande aventure.  
C'est en semant des fleurs que pour nous tout a commencé... 