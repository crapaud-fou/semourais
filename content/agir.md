﻿---
Title: Planter des forêts
Weight: 1
Icon: fa-th
---

Changements climatiques, étés caniculaires, les arbres ont le vent en poupe. 
Il faut planter des arbres. Cela ne fait plus de doute.
Mais, comme nous, les arbres ont besoin de soin et d'un environnement où ils sont bien. 
C'est pourquoi, pour planter nos forêts, nous adaptons la méthode Miyawaki qui a mille fois fait ses preuves au Japon. 
Cette technique repose sur une minutieuse préparation du sol et une plantation très dense d'arbres de nombreuses espèces.  
Résultat: les arbres, tous ensemble, grandissent plus fort et plus vite. Ces forêts constituent un formidable espoir pour lutter contre les changements climatiques et restaurer la biodiversité.En cliquant sur les images ci-dessous, vous découvrirez des moyens d'agir à votre tour.Nous les avons expérimenté et partageons avec vous nos expériences.

{{<agir >}}