---
# Title: Intro
Weight: 1
# Icon: fa-envelope
# Img: img/about.jpg
---
**Face à la disparition des abeilles, à la destructions des forêts, aux changements climatiques, nous, les Semourais, avons décidé d'agir sans attendre. Nous avons rassemblé autour de nous, pour agir ensemble, prouver que c'est à la portée de tous et partager nos expériences.Nous plantons des forêts denses, riches en biodiversité, qui seront notre meilleur allié pour lutter contre les changements climatiques, la pollution et les canicules urbaines.Et pour aider à la naissance de ces forêts, avec ceux qui se sentent prêts ,nous fabriquons de la terre fertile, créons des pépinières et semons des prairies fleuries.** 