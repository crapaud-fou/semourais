﻿---
Title: A propos des Semouraïs
Weight: 2
Icon: fa-envelope
Img: img/about.jpg
---
Fondée en juin 2018 et située à Marseille, l’association Semourais (semeurs de biodiversité) opère en synergie avec de nombreux acteurs locaux.    
L’association Semourais agit et encourage les actions favorables à un retour d’une plus grande biodiversité sur nos territoires.  
Si son objectif principal est de planter des forêts denses, riches en biodiversité, inspirées de la méthode Miyawaki, elle partage ses activités entre :

* semis de prairies fleuries ;
* recyclage et restauration de sols vivants ;
* plantations de forêts.

Principaux Partenaires
Articulteurs
Société d’Horticulture et d’Arboriculture des Bouches du Rhône 
Le Talus 
Crapauds Fous

{{<teams >}}