---
Title: LANCER SA PROPRE PEPINIERE PARTICIPATIVE
Categories: [creer-une-pepiniere]
Img: img/pepiniere.jpg
---


# Objectif
 
 

L’objectif d’une pépinière participative est simple :
alimenter en plants d’un an et demi à deux ans nos forêts plantées avec [la méthode Miyawaki](https://wiki.crapaud-fou.org/La-m%C3%A9thode-Miyawaki), afin de faire chuter leur coût et planter plus de forêts.


Vidéo idéale pour sensibiliser les enfants : 
<center>
{{% youtube cfZTzsQ4gEs %}}
</center>


[Ces forêts “sanctuaires de biodiversité”](https://wiki.crapaud-fou.org/Des-for%C3%AAts-Miyawaki-sanctuaires-de-biodiversit%C3%A9) sont composées de très nombreuses espèces (30 à 50), mais une pépinière participative ne cherchera pas à toutes les cultiver. Nous pensons qu’il est plus judicieux qu’**une pépinière** se concentre sur la culture de **5 à 7 espèces** disponibles localement. 
 

# Choix du lieu
 
 
 
Pour lancer une pépinière il vous faut un lieu disposant de deux éléments indispensables :


*  Un espace offrant de **l’ombre** à peu près toute la journée. Typiquement, sous de grands arbres ou à l’ombre d’une haute haie bien orientée ou d’un mur bien orienté. une surface de 2 ou 3 mètres carrés (2m x 1m ou 3m x 1m) suffit ! 
*  Un **accès d’eau**.

Si vous avez trouvé un tel lieu, sachez tout de suite qu’une fois lancée, votre pépinière ne vous demandera que peu d’entretiens : 20 à 30 minutes par semaine environ pour l’arrosage et un peu de désherbage de temps en temps.
 

# Matériel de base
 
 
 
Avant de vous lancer il vous faut un minimum de matériel que vous pouvez facilement emprunter ou trouver gratuitement :

* des bouteille d’eau en plastique qui constitueront vos pots ! Et oui ! Coupées (à la base du cône, entre le bouchon et sa partie cylindrique), puis percée à sa base de quelques trous (de 5 à 7 mm de diamètre environ), elles formeront un contenant parfait (étroit et profond) pour de jeunes plants d’arbres. Quant au travail de découpe et perçage, il peut être réalisé par des enfants.
* des cagettes en bois (les commerçants vendant des fruits et légumes les jettent très régulièrement)
* une pelle, une pioche ou un outil vous permettant de creuser et gratter la terre
* un arrosoir ou tuyau d’arrosage
* du compost mûr (optionnel)
* des étiquettes pour inscrire les noms des essences (optionnel).

 
# Les techniques


 
Il existe trois principales techniques pour cultiver de jeunes plants.


*  **La transplantation**

Il s’agit tout simplement de prélever dans la nature un jeune plant qui a déjà germé.  
Beaucoup d’arbres poussent dans la nature en des endroits où nous savons qu’ils n’auront pas la possibilité de grandir.  
En les prélevant pour votre pépinière, vous les sauvez ! Ils ont en général entre 6 mois et un an voire un peu plus et pourront très vite participer à une plantation de forêts.  
C’est la méthode que nous utilisons autant que possible et que nous vous recommandons. 

*  **Le semis**

C’est la plus évidente et la plus connue, mais aussi la plus difficile à mettre en oeuvre.  
Il s’agit de récolter des graines d’arbres et de les semer.  
Les difficultés sont multiples et très variables d’une espèce à une autre.  
Tout d’abord, les arbres ne produisent pas leurs fruits au même moment. Ensuite et surtout, il faut lever la dormance de la graine qui, sans cela, reste endormie et ne germe pas. Les graines attendent des conditions bien précises pour germer. Parfois, elles doivent d’abord subir une période de froid. D’autres doivent passer par le tube digestif des oiseaux ou encore par le feu.  
Le semis est une méthode très prisée par les néophytes et les enseignants, mais c’est la méthode la plus difficile.  
Ces résultats sont très aléatoires !  
Si vous êtes enseignant, justement, considérez qu’elle a un but pédagogique : l’observation des transformations qui, d’une graine, produisent un plant. Mais si vous voulez créer une pépinière qui participe à une plantation, il sera beaucoup plus simple et gratifiant d’utiliser la première technique : la transplantation.

*  **Le bouturage**  

“A la sainte Catherine tout bois prend racine” est une expression souvent détournée de son sens premier.  
On pense que ce dicton encourage à planter des arbres fin novembre. C’est un bon moment en effet, mais la période de plantation de jeunes plants avec des racines est en réalité beaucoup plus étendue. Vous aurez aussi de très bons résultats en plantant en octobre par exemple.  
Ce dicton, originellement,  évoque le bouturage. Le bois qui prend racine est une petite branche qui, mise en terre, crée des racines. Cette technique est plus simple que le semis, mais sa réussite varie d’une espèce à une autre. Elle peut néanmoins être utile si vous ne trouvez pas de jeunes plants à transplanter.  
Elle a une particularité par rapport au semis et à la transplantation : le plant produit n’a pas subi le brassage génétique de la reproduction.  
C’est un clone de son parent. C’est un avantage si vous voulez réaliser des expériences sur l’effet d’un environnement sur une espèce. Tous les plants testés ayant le même patrimoine génétique, vous mesurez sans filtre les effets de l’environnement sur le développement et les mutations de votre plante.  
Par contre, dans la perpective d’un reboisement diversifié, cette pauvreté génétique sera un inconvénient. Si un de vos plants bouturés porte une maladie, tous ses frères clonés seront frappés.



## LA TRANSPLANTATION


 
Vous avez trouvé un endroit pour votre pépinière et rassemblé le matériel présenté plus haut.  
Vous êtes prêts pour lancer une pépinière avec la méthode à la fois la plus simple et la plus rapide : la transplantation.  
On a testé et on confirme ! C’est très facile !  

Avant de vous lancer, sélectionnez un lieu où poussent de beaux et grands arbres.  
Assurez-vous qu’ils sont bien “indigènes”, c’est-à-dire de la région, afin qu’ils puissent participer à [une plantation de forêt Miyawaki](https://wiki.crapaud-fou.org/Kits-Semourais-pour-passer-%C3%A0-l-action-et-planter-des-for%C3%AAts).  
Evitez les espèces invasives et, en cas de doute, n’hésitez pas à [nous contacter](https://wiki.crapaud-fou.org/Semourais-en-bref#Nous_contacter). 

Pour passer à l’action, l’idéal est maintenant **d’attendre la pluie** qui rendra votre terrain beaucoup plus meuble.  
**Le lendemain d’un jour de pluie**, si possible entre mi-octobre et mi mars (jusque mi-avril pour le Nord de la France), cherchez de jeunes plants aux pieds des arbres semenciers adultes. Important : la forme des feuilles des jeunes pousses est souvent différente de celle des feuilles des arbres adultes.

<center>
{{% img src=img/fiches/pepiniere/jeucroise.png %}}
</center>

N’hésitez pas à vous promener. Les semences voyagent et les jeunes plants sont peut-être un peu plus loin.  
Vous avez trouvé un bébé arbre ! Il doit mesurer **entre 8 et 30-40 cm grand max**. Pas plus ! 
Par exemple, dans cette série de photos, deux des plants ont la taille maximum !

Article 1402231173 1491423845Chene Plant 2DSCF0417Hetre Plant

En pratique, il suffit de **creuser un trou d’une profondeur à peu près égale à la taille de l’arbre (ou un peu plus)** et de délicatement le déraciner.
Si la terre est bien meuble et que vous pouvez **récupérer la motte avec les racines**, c’est encore mieux !
Sinon, récupérez un peu de la terre dans laquelle votre bébé arbre a poussé.
C’est très important. Elle contient une espèce de champignon microscopique qui l’aide à grandir. 
Si vous mettez votre jeune plant et son champignon dans le même pot votre arbre grandira deux fois plus vite ! 
C’est l’effet “champignon magique”. Pour les savants cela s’appelle la [mycorhize](https://fr.wikipedia.org/wiki/Mycorhize) , un [domaine de recherche en pleine croissance](http://www.inra.fr/Grand-public/Ressources-et-milieux-naturels/Tous-les-dossiers/Metagenomique-du-sol/La-mycorhize/(key)/1) dont cette [vidéo](https://www.youtube.com/watch?v=lsj9EhZJiL4) vous donnera une bonne idée de l’importance. 

Si vous avez collecté une quinzaine de plants, c’est déjà un bon début !

Rassemblez vos plants et **préparez vos pots-bouteilles**.
Il est encore temps de percer le fond de vos bouteilles, si ce n’est pas déjà fait. Comme la découpe de la bouteille, le perçage des trous peut se faire avec une simple paire de ciseaux ou un couteau bien affuté. Si vous devez préparer beaucoup de pots-bouteilles, vous pouvez les percer à la chaîne avec un tournevis ou une clef à six pans chauffés. La chaleur facilite la rapidité et la netteté du perçage. 
Au fond de votre pot-bouteille, jetez 2-3 cm de petits cailloux, graviers, tessons de pots en terre, billes d’argile ou coquilles d’huîtres. Ils sont là pour faciliter le **drainage** c’est-à-dire l’écoulement de l’eau. 


Billes DArgile 8 16mm Fibre VerteBroken 3574822 960 720Oysters 269471 960 720Pebbles 3429815 960 720

Cela limitera le risque que votre plant se noie (s’asphyxie ou pourrisse...).

Ensuite, remplissez un quart (1/4) ou un tiers (1/3) de votre pot-bouteille avec la terre récoltée au pied de votre plant.
Vous pouvez intégrer un peu de compost mûr si vous en avez à disposition (entre 30 et 50%). C’est un plus, mais ce n’est pas vital pour votre arbre. 

Image
Placez votre plant dans votre pot-bouteille.
Complétez avec de la terre (ou un mélange terre-compost mur) jusqu’à deux ou trois cm du haut de la bouteille.

**Paillez**. Le paillage consiste simplement à déposer un peu de paille, de foin, des feuilles ou du BRF (bois raméal fragmenté = des morceaux de bois broyés par les élagueurs) à la surface de votre terre.

Image
Le paillage limitera l’évaporation en surface et protègera vos plants du vent, de la chaleur et du froid. 

**Rassemblez vos pots dans une cagette** : vos pots-bouteilles se caleront et tiendront mutuellement. 

Au moment d’installer votre cagette dans votre pépinière, coupez son fond. Celui-ci risquerait d’accélerer la dégradation de votre cagette. Les pots-bouteilles, dans l’idéal, doivent être déposés sur un épais tapis de feuilles. Il vous permettra de ne pas arracher les racines de vos plants à leur sortie.

Arrosez généreusement. 

Un petit résumé en images ? C’est parti !


IMG 0259IMG 0260IMG 0261IMG 0262IMG 0263IMG 0264IMG 0265

 

 

 

 

 


1.  Photo 1: plant prélevé. Notez la taille impressionnante des racines !
2.  Photo 2: bouteilles coupées et percées avec graviers pour le drainage
3.  Photo 3: terre prise sur place sur un tiers de la bouteille
4.  Photo 4: mise en place du plant
5.  Photo 5: terre jusque 2 à 3 cm du bord
6.  Photo 6: paillage
7.  Photo 7: Cagette , pour le transport vers la pépinière et pour caler les pots-bouteilles sur place


*Avantages de la transplantation* 

*  vous gagnez 6 à 18 mois !!
*  vous récoltez des plants dans une terre qui leur convient avec leurs “champignons amis” (mycorhize).
*  c’est la méthode la plus simple ! Moins de travail et gros taux de réussite (autour de 90%).

*Inconvénients* 

Il faut creuser pour déraciner vos plants, d’où l’importance d’attendre la pluie qui ameublit la terre. 


### Entretien de la pépinière



Il est très simple.

*  **Arrosage** : une fois par semaine  (2 fois au début si vous voulez bichonner vos bébés ou en période de canicule) arrosez abondamment vos pots. 
*  **Désherbage** : une fois par mois environ, retirez les herbes qui viennent concurrencer vos jeunes plants dans les pots-bouteilles. 


### Adapter sa pépinière aux conditions extrêmes : canicule ou grand froid



Le plastique n’est pas un isolant thermique. Si vos pots-bouteilles sont à l’air libre, vos plants vont souffrir quand les températures vont s’envoler l’été ou chuter l’hiver, surtout s’il gèle. 
Pour les protéger sans le recours d’une serre, il vous faudra reprendre vos outils et creuser un peu le sol de votre pépinière. 

Au minimum, creusez de quoi enterrer vos pots au 3/4 et tapissez le fond de votre trou d’une bonne épaisseur de feuilles mortes (5 cm minimum).
Si vous êtes nombreux, motivés et bien équipés, creusez vingt centimètres de plus et tapissez le fond de votre fosse avec du carton (type “carton de déménagement”). Vous pouvez en mettre jusqu’à une épaisseur de 15 cm ! Ce carton, riche en carbone, attirera la microfaune du sol et des champignons qui aideront la croissance de vos arbres. Il constituera aussi un tapis mou qui s’émiettera au moment du retrait de vos pots-bouteilles. Par dessus vos cartons, jetez une couche de 3 à 5 cm d’un mélange de compost ou broyat si vous en avez à disposition, puis une couche de 5 à 10 cm de feuilles mortes.

Déposez vos pots-bouteilles sur votre tapis de feuilles mortes.
Répartissez la terre, sortie du trou que vous avez creusé, entre les pots-bouteilles afin qu’il n’y ait plus d’air entre vos pots, puis autour des pots-bouteilles.  

Ainsi enterrés, vos pots-bouteilles seront protégés du **chaud** et du **froid** par la terre, qui est un excellent isolant thermique, mais aussi de la **lumière**.


### Préparation des pots à l’approche de la plantation de la forêt



Au bout d’1 an maximum (voire, 2 ans si vos bébés arbres proviennent du semis de graines), le grand jour approche pour vos jeunes plants. La date de leur plantation dans une forêt n’est plus très loin...

Elle se situera entre mi-octobre et mi-mars si vous êtes dans le Sud de la France, mi-octobre et mi-avril pour l’Ouest (climat océanique). Pour le Nord et l’Est vous aurez deux créneaux plus courts : mi-octobre à fin-novembre et mars-mi avril. 

**Planter avant l’hiver est idéal**, car c’est une période où ce sont les racines qui grandissent, les feuilles ne reprenant leur croissance qu’au printemps. 

Bref, la date de la plantation est fixée. La première chose à faire est de séparer les jeunes arbres qui seront plantés de ceux qui ne le seront pas encore, pour les mettre un peu plus au soleil (mi-ombre voire 30 ou 40% d’ombre), afin qu’ils s’habituent à recevoir plus de rayons solaires. 

Si les pots-bouteilles ont été enterrés, il est très probable que vos plants aient lancé des racines dans la terre. Essayez de retirer vos plants avec un maximum de racines. Cela améliorera leur reprise. 

Si c’est possible, trois semaines avant leur transplantation, installez-les sur le terrain où ils seront plantés afin qu’ils s’acclimatent au lieu où ils seront amenés à grandir. 

Pour la plantation, référez-vous à notre [kit pour planter des forêts Miyawaki](https://wiki.crapaud-fou.org/Kits-Semourais-pour-passer-%C3%A0-l-action-et-planter-des-for%C3%AAts?structure=Semourais). 

Si vous n’avez pas trouvé de jeunes plants près des arbres semenciers adultes, deux autres options s’offrent à vous : le semis ou le bouturage.

 

## LE SEMIS


 
Econome en énergie (on n’a pas besoin de creuser...), c’est une méthode qui demande plus ou moins d’expertise en fonction de l’espèce que l’on veut faire pousser.

Nous avons sélectionné pour vous une liste d’espèces pour laquelle la réalisation du semis est assez facile.  


### Matériel spécifique pour le semis :


*  Un ou des bacs transportables de 20 à 30 cm de fond.
*  Un grillage à maille fine (type grillage à poule) ou un filet de protection.
*  Du sable.


### Semis de graines d’arbres pour débutant, les espèces :


*  Pour toute la France, Chêne, Hêtre, Frêne, Erable, Noyer sauvage et Abricotier sauvage.
*  Pour le sud (et territoires hors gel l’hiver), vous pouvez ajouter Grenadier, Jujubier et Amandier.
*  Pour le reste du pays des espèces plus rustiques : Noisetier, Pommier sauvage et Poirier sauvage.

[En cliquant ICI](https://wiki.crapaud-fou.org/Fiches-arbres-Semourais), vous ouvrez des fiches arbres où vous trouverez des précisions sur, comment reconnaître ces espèces, quand récolter ses graines et d’éventuels conseils spécifiques pour améliorer vos chances de réussir votre semis. Pour une identification plus précise des espèces, vous pouvez vous référer à [cette encyclopédie libre](http://www.snv.jussieu.fr/bmedia/arbres/index.htm) et à [ce site](http://etienne.aspord.free.fr/) pour les résidents des Bouches du Rhône (berceau de notre projet) où vous trouverez de très nombreuses espèces et des clefs de reconnaissance plus traditionnelles.

**Important** : s’agissant des **fruitiers**, ce sont dans l’idéal des fruitiers sauvages dont vous devez collecter les graines. Assurez-vous, par une observation minutieuse de l’arbre (ou auprès d’une personne compétente), qu’il ne s’agit pas d’un arbre greffé.

Pourquoi ?
La logique de la greffe (normalement observable) est très simple. On greffe la branche d’une espèce à gros fruits goûteux sur le tronc d’une espèce robuste.

**Conséquence** : si vous collectez des fruits d’un arbre greffé, vous aurez des arbres inadaptés à votre sol et fragiles.
Même chose si vous semez des graines ou noyaux de fruits achetés sur le marché ou chez un épicier. 

Cela peut marcher mais on sort de la démarche Miyawaki, qui privilégie les espèces vraiment adaptées à un environnement.

Pour les espèces ne figurant pas dans cette liste, la difficulté du semis provient de la dormance, période pendant laquelle la graine est en sommeil, qu’il est parfois délicat de lever. Vous pouvez néanmoins essayer avec les conseils généraux qui suivent ou adapter ce protocole en faisant quelques recherches (auprès de spécialistes ou sur internet). 

Si vous obtenez de bons résultats sur une espèce n’appartenant pas à la liste, [contactez-nous](https://wiki.crapaud-fou.org/Semourais-en-bref#Nous_contacter) pour en faire profiter la communauté des planteurs Semourais ! 


### Se promener, observer, repérer :

Pour réaliser un semis, il vous faut d’abord récolter des graines. Repérez autour de chez vous, des arbres figurant dans la liste précédente. Si vous n’êtes pas sûr de les reconnaître, cliquez sur leurs noms et découvrez à quoi ils ressemblent.

Listez les espèces autour de chez vous et, à l’aide des informations données dans nos fiches arbres, faites vous un petit calendrier de ramassage. 

Exemple : Hêtre, entre 20 septembre et 20 novembre. Chêne et Noyer : octobre...


### Préparer un bac de semis :

Pour faire germer des graines d’arbres, il vous faut un ou des bacs assez profonds : **entre 20 et 30 cm** dans l’idéal.
Si vous [manquez de profondeur](https://www.youtube.com/watch?v=hjOUwdLTmGA), les racines de vos jeunes plants ne pourront pas assez s’enfoncer dans votre terre.
Si votre bac est très profond, pas d’inconvénient pour vos arbres, par contre son remplissage sera plus long...  

Pour cela, vous pouvez récupérer des bacs plastiques, des caissettes, des cagettes ou encore construire un grand bac avec des palettes ou des planches. Pour les bricoleurs, un exemple en [vidéo](https://www.youtube.com/watch?v=78EuXuBzE_0). 

Le **fond** de votre bac doit être **percé de trous** permettant à l’eau de s’évacuer, sinon l’eau stagnante risque de faire moisir vos graines. S’il n’y en n’a pas, faites les... Des trous d’1 cm de diamètre conviendront très bien. 

Placez votre bac au nord, afin qu’il soit en permanence à l’ombre. Choisissez un emplacement abrité du vent.

Couvrez le fond de votre bac d’une épaisseur de 2 cm de petits cailloux, graviers, tessons de pots en terre, billes d’argile ou coquilles d’huîtres. Ils permettront le drainage de l’eau au moment de l’arrosage. 

Puis, remplissez votre bac d’un mélange dit “léger” composé de 1/3 de terre et 2/3 de sable. 

Inutile d’ajouter du compost. Lors de la germination, l’énergie qui fera pousser votre bébé arbre viendra d’abord des réserves de sa graine.

Il est aussi important de prévoir un grillage ou un filet pour protéger vos graines des chats et des oiseaux. Si vous optez pour un grillage, choisissez le fin, type “grillage à poule”. Ce grillage ou filet sera destiné à recouvrir la surface de votre bac. 


### Collecte et semis :

Nous avons sélectionné des espèces (voir la liste plus haut) qu’on peut semer rapidement après leur collecte. 

**Ramassez les graines, glands ou fruits** (en fonction de l’espèce).

**Important !** En même temps que les graines, **récoltez de la terre** à l’endroit où vous avez ramassé vos graines. Lors de la transplantation, cette terre permettra de favoriser la mycorhize de vos jeunes plants, indispensable à leur bon développement.

Sur les fiches d’arbres (voir plus haut), vous trouverez parfois des conseils pour sélectionner les meilleures “graines”. Intéressant, si vous en avez ramassé beaucoup ! 

Une fois rassemblées les graines que vous voulez semer, **laissez les tremper 24h dans de l’eau froide**. 
Cela permettra à la graine de se ramollir et donc de germer plus facilement. Si vous êtes en retard dans la préparation de votre bac de semis, cela vous laissera un jour de plus pour le finaliser.

Jetez les graines d’une même espèce dans un bac ou une partie de votre bac. Enfoncez les d’1 ou 2 cm **dans la position où elles sont tombées**. 

Une fois toutes vos graines en terre, arrosez doucement : il faut juste que votre mélange terre-sable soit humide. 
N’arrosez pas trop la première fois, ni les fois suivantes, ni trop souvent !  Sinon vos graines risquent de pourrir... 

Pour que votre mélange terre-sable garde mieux l’humidité, vous pouvez le **pailler**. 

N’arrosez que quand votre terre au toucher est sèche, toujours avec parcimonie. 

Enfin, protégez vos graines des oiseaux et des chats en couvrant votre bac avec **un grillage** (assez fin, un “grillage à poule” par exemple) placé au moins 5 cm au dessus de la surface de votre terre. 

Les oiseaux... et les chats !?? 
Oui : les oiseaux qui auraient envie de picorer vos graines... et les chats qui risquent de prendre votre bac pour une litière... et y faire leurs besoins...

Patience et... transplantation :
 

Il vous faut maintenant vous armer de patience. 

Tout d’abord, parce que certaines graines vont attendre que les températures se réchauffent pour germer. Elles sont alors **en dormance**. 
D’autre part, quand vos graines germeront, ce sont d’abord les racines qui pousseront. 
Il vous faudra donc attendre plusieurs semaines avant de voir apparaître les parties aériennes de vos bébés arbres. Autrement dit, pas d’inquiétude si vous ne voyez rien en surface.

Conservez votre mélange de terre et de sable légèrement humide et faites confiance à la nature.

Au début du printemps, quand vos jeunes pousses auront formé trois ou quatre feuilles, vous pourrez les mettre dans des pots-bouteilles individuels, en suivant les conseils donnés dans le paragraphe transplantation. 

Cette vidéo vous permet de visualiser la plupart de ces étapes :  [vidéo de Damien : semer des forêts](https://www.youtube.com/watch?v=LF4R7upTay0) et de découvrir comment vous pouvez également semer directement dans la terre d’un jardin. 

*Avantages du semis*

*  pas besoin de se fatiguer à creuser...
*  si on maîtrise la technique pour une espèce, on peut facilement produire des centaines de plants !
*  les jeunes plants grandissent avec leur racine mère - qu’on appelle racine pivot - qui n’a aucune raison d’être abîmée lorsqu’on procède ainsi.
*  ludique, expérimental, pédagogique pour les enfants. 

*Inconvénients*

*  la méthode et le pourcentage de réussite varient d’une espèce à l’autre
*  on perd 1 an par rapport à la transplantation ! Un an d’entretien pendant lequel les jeunes plants sont fragiles et demandent des soins simples mais réguliers...

 

## LE BOUTURAGE



Pour certaines espèces, on peut opter pour le bouturage (reprise d’un plant à partir d’un morceau de branche). C’est une technique qui revient à **cloner un plant** dont on coupe une petite branche. 

Pour commencer, il faut **découper en biseau** une branche latérale sans fleur.

Il faut ensuite **couper la plupart des feuilles**. En laisser deux ou trois aux extrémités de votre bouture. 

Badigeonnez le bout de votre bouture avec **une hormone de croissance** avant de la planter dans un pot composé d’un mélange du même type que pour les transplantations (ou un peu plus drainant “1/2 sable - 1/2 compost mur” voire 2/3 sable).

Bouturées en Septembre, certaines espèces comme le laurier sauce sont capables de former des racines en deux mois seulement. Vous aurez alors un jeune plant et pourrez suivre les conseils donnés dans la section transplantation.

Exemples :

*  [bouturage d’un laurier sauce](https://www.youtube.com/watch?v=ZZRrWB4eq6I)
*  [bouturage d’un hortensia dans notre pépinière Semouraïs de Bretagne](https://tube.crapaud-fou.org/videos/watch/07ff9331-8be8-4e86-b5f7-d21656ce4d53).


*Avantages du bouturage*

*  Quand on maîtrise la technique pour une espèce, elle permet d’obtenir rapidement de nombreux plants.
*  On n’a pas à creuser...

*Inconvénients* 

*  la réussite est variable et dépend fortement de l’espèce
*  les plants obtenus sont des clones du plant dont ils sont tirés. Il ne faudra donc pas les planter ensemble dans une même forêt car ils risqueraient de tous tomber malades en même temps. 

=> Si de nombreux clones sont obtenus d’un même plant, il faudra donc les répartir entre plusieurs forêts Miyawaki.

 
 
 
# REMARQUE
 
C’est une très mauvaise idée de vouloir planter / transplanter de grands arbres. 

S’ils sont tout de suite impressionnants, ils auront beaucoup de mal à reprendre.

En quelques années, ils seront rattrapés par de jeunes arbres à la repousse vigoureuse, plus résistants et beaucoup moins chers !