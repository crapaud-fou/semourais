---
Title: Amandier
Img: /img/fiches/amandier/arbre-1.jpg
categories: [planter-une-foret, creer-une-pepiniere]
---
# Objectifs de cette fiche: 
* Reconnaître un amandier adulte. 
* Reconnaître un très jeune amandier à prélever pour votre pépinière. 
* Vous guider dans le semis d’amandiers.  

Pour plus de détails sur les semis d’arbres et la transplantation de jeunes plants, se reporter au Kit semourais pour monter une pépinière participative.  
IMPORTANT !  
L’amandier est un arbre fruitier. Assurez-vous que vous avez affaire à un amandier sauvage, c’est-à-dire un arbre non greffé.

# Arbres adultes
L’amandier est un arbre dont les fleurs précèdent les feuilles, une des sources de sa beauté. 
{{< gallery class="content-gallery" >}}
  {{< img src="/img/fiches/amandier/arbre-1.jpg" >}}
  {{< img src="/img/fiches/amandier/arbre-2.png" >}}
  {{< img src="/img/fiches/amandier/arbre-3.jpg" >}}
{{< /gallery >}}

# Apparence des feuilles et du fruit
{{< gallery class="content-gallery" >}}
  {{< img src="/img/fiches/amandier/feuille-1.jpg" >}}
  {{< img src="/img/fiches/amandier/feuille-2.jpg" >}}
  {{< img src="/img/fiches/amandier/feuille-3.jpg" >}}
{{< /gallery >}}

# Jeunes plants
Ces plants ont l’âge d’être mis en pot par transplantation dans votre pépinière.  
Pour plus de détails sur la méthode, reportez-vous à notre Kit semourais pour monter une pépinière participative
{{< gallery class="content-gallery" >}}
  {{< img src="/img/fiches/amandier/pousse-1.jpg" >}}
  {{< img src="/img/fiches/amandier/pousse-2.jpg" >}}
{{< /gallery >}}

# Période de collecte des graines

Fin d’été: mi-août à mi-Septembre

# Conseils pour planter des amandes
* Vous pouvez appliquer à la lettre les conseils donnés dans notre kit pépinière pour les semis d’arbres. 
* Vos amandes seront en **dormance** pendant l’hiver, donc pas d’inquiétude si vous ne les voyez pas germer, c’est normal. Pendant ce temps leurs coques se ramollissent. Elles commenceront à germer au début du printemps.