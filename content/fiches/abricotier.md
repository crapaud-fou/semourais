---
Title: Abricotier
Img: /img/fiches/abricotier/arbre-1.jpg
categories: [planter-une-foret, creer-une-pepiniere]
---
# Objectifs de cette fiche
* Reconnaître un abricotier adulte. 
* Reconnaître un très jeune abricotier à prélever pour votre pépinière. 
* Vous guider dans le semis de noyaux d’abricot.

Pour plus de détails sur les semis d’arbres et la transplantation de jeunes plants, se reporter au Kit semourais pour monter une pépinière participative. 
IMPORTANT !  
L’abricotier est un arbre fruitier. Assurez-vous que vous avez affaire à un abricotier sauvage, c’est-à-dire un arbre non greffé.

# Arbres adultes
{{< gallery class="content-gallery" >}}
  {{< img src="/img/fiches/abricotier/arbre-1.jpg" >}}
  {{< img src="/img/fiches/abricotier/arbre-2.jpg" >}}
  {{< img src="/img/fiches/abricotier/arbre-3.jpg" >}}
{{< /gallery >}}
# Apparence des feuilles et du fruit
{{< gallery class="content-gallery" >}}
  {{< img src="/img/fiches/abricotier/feuille-1.jpg" >}}
  {{< img src="/img/fiches/abricotier/feuille-2.jpg" >}}
  {{< img src="/img/fiches/abricotier/feuille-3.jpg" >}}
{{< /gallery >}}

# Jeunes plants
Ces plants ont l’âge d’être mis en pot par transplantation dans votre pépinière.  
Pour plus de détails sur la méthode, reportez-vous à notre Kit semourais pour monter une pépinière participative
{{< gallery class="content-gallery" >}}
  {{< img src="/img/fiches/abricotier/plant-1.jpg" >}}
  {{< img src="/img/fiches/abricotier/plant-2.jpg" >}}
{{< /gallery >}}

# Période de collecte des graines
Juillet et Août

# Conseils pour planter des noyaux d’abricot
* Vous pouvez appliquer à la lettre les conseils donnés dans notre kit pépinière pour les semis d’arbre.
* Vos noyaux d’abricot seront en dormance pendant l’hiver, donc pas d’inquiétude si vous ne les voyez pas germer, c’est normal. Pendant ce temps leurs coques se ramollissent. Ils commenceront à germer au début du printemps.