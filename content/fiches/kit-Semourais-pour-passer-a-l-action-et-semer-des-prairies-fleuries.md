---
Title: Kit Semourais pour passer à l action et semer des prairies fleuries
Categories: [semer-une-prairie]
Img: /img/fiches/semis.jpg
---
Mode d’emploi du semis de prairies réalisé à partir des conseils de Thomas MARTIN (paysagiste et articulteur) et Eric MULLARD (ancien formateur aux métiers du paysage).

# Saisons des semis 
Septembre/octobre ou Mars/avril (remarque: dans le sud de la France, précocement chaud et sec, semer plutôt début mars)

# Préparation du sol 
1. Repérez l’espace que vous voulez semer.
2. Y disposer, si possible, un couvert organique d’une dizaine de cm, deux à trois mois avant le semis. Il pourra être constitué de feuilles mortes, BRF (Bois Raméal Fragmenté), carton, paille… 
Une prairie est peu gourmande en “azote”, ces fleurs ne feront pas de gros légumes... ;) , inutile donc de charger votre couvert en compost ou fumier.
3. Quelques heures ou jours avant le semis, décompactez votre sol avec une fourche ou une grelinette afin de l’aérer. Evitez autant que possible de retourner la terre afin de préserver la vie du sol.  
_Remarque: si vous n’avez pas pu déposer un couvert organique plusieurs semaines à l’avance et que votre sol est vraiment très compact ou dégradé (très peu de végétation apparente), ne vous lancez pas dans une grande prairie ! (6 à 10m2 seront plus raisonnables) et amendez votre sol avec une fine couche de compost (environ 2cm) à incorporer à votre terre avant le semis._
4. Griffez votre sol.
5. Le ratisser et le niveler.
Si vous voulez une prairie avec un maximum de fleurs, réalisez ce qu’on appelle un « faux semis ». 
Arrosez légèrement, chaque soir, pendant une dizaine de jours. Les graines d’adventices en dormance germeront. 
Après dix ou quinze jours, griffez votre sol pour vous débarrasser de vos adventices. Votre espace est prêt à accueillir votre semis de prairie. 
Important : éviter de semer un jour où il y a du vent !

# Préparation des graines 
1. Compter 5 grammes de graines (l’équivalent d’une pleine cuillère à café) pour 2 mètres carrés de prairies. Vous verrez, cela ne représente pas grand chose. C’est la raison pour laquelle il faut les mélanger à du sable ou un terreau fin et sec. Cela permettra un semis à la volée plus homogène.
2. Bien mélanger ce substrat à vos graines.

# Semis 
1. Au besoin (si vous avez réalisé un faux-semis), re-décompacter rapidement le sol
2. Semer à la volée (si vous ne connaissez pas le geste, vous pouvez regarder  les vidéos ”jardiniers en action” un peu plus bas).  
{{< gallery class="content-gallery" >}}
  {{< img src="/img/fiches/semis.jpg" >}}
{{< /gallery >}}  
{{< cratube b34c1e39-51e8-4cb3-9faa-c628a6d3dade >}}  
{{< cratube c418b4a6-09d8-49af-899e-d10ff5cfa9de >}}
3. Ratisser pour répartir les graines
4. Tasser avec un rouleau ou en tapotant avec un râteau
5. Arroser légèrement (cf les vidéos en lien pour le geste), quotidiennement pendant une dizaine de jours puis, régulièrement, s’il fait chaud et sec.

Vidéos “ jardiniers en action” 
{{< youtube DNdhk7io_Rc >}}
{{< youtube dhag46kQsKo >}}
{{< youtube lY81TIci8Yw >}}

Conseils de notre fournisseur de graines  
http://bertrand-flowers.com/nos-guides-techniques/reussir-le-semis-des-melanges-de-fleurs/

