# Semouraïs

Site web de l'association Semouraïs.

## Structure du site

```mermaid
graph LR;
  Home --> planter-une-foret;
  Home --> fabriquer-sa-terre;
  Home --> creer-une-pepiniere;
  Home --> semer-une-prairie;
  planter-une-foret --> actions_puf[Actions];
  planter-une-foret --> fiches_puf[Fiches];
  fabriquer-sa-terre --> actions_fst[Actions];
  fabriquer-sa-terre --> fiches_fst[Fiches];
  creer-une-pepiniere --> actions_cup[Actions];
  creer-une-pepiniere --> fiches_cup[Fiches];
  semer-une-prairie --> actions_sup[Actions];
  semer-une-prairie --> fiches_sup[Fiches];
```

## Development

### 1ere fois

#### Installer pré-requis

* Git : https://git-scm.com/downloads
* Hugo : https://gohugo.io/getting-started/installing/

#### Recupérer le projet

Aller dans un répértoire à vous _(du style documents sous windows)_

```
git clone https://gitlab.com/crapaud-fou/semourais.git
cd semourais
git submodule update -i
```

### Afficher le site

```
hugo serve -b http://localhost:1313
```

ouvrir l'adresse `localhost:1313` sur son webbrowser

## Edition
### 1ere page
#### Introduction
##### Remplacement image
Il suffit de remplacer l'image `static/img/banner.jpg`
##### Changement du texte
Il faut modifier le texte dans le fichier `content/index.md`
#### Portfolio
##### Changement du texte
Il faut modifier le texte dans le fichier `content/portofolio.md`
#### About
##### Changement de l'image
Il faut modifier le paramètre `Img` dans le fichier `content/about.md`
##### Changement du texte
Il faut faut modifier le texte dans `content/about.md`
#### Contact
##### Changement du texte
Il faut faut modifier le texte dans `content/contact.md`
### Fiches ou Actions
Les fiches sont toutes dans le répertoire `content/fiches`.  
Et les actions sont toutes dans le répertoire `content/actions`.
#### Choix de la categorie
Il faut mettre dans le paramètre `Categories` une (ou plusieurs valeurs parmis)
* planter-une-foret
* fabriquer-sa-terre
* creer-une-pepiniere
* semer-une-prairie

# Source des images
* [banner.jpg](static/img/banner.jpg) : Tuo un crapaud-fou
* [foret.jpg](static/img/foret.jpg) : Image par<a href="https://pixabay.com/fr/users/Schwoaze-4023294/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3616194">Schwoaze</a> de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3616194">Pixabay</a>
* [pepiniere.jpg](static/img/pepiniere.jpg) : Image par [JamesDeMers](https://pixabay.com/fr/users/JamesDeMers-3416/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=60830) de [Pixabay](https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=60830)
* [terre.jpg](static/img/terre.jpg) : Image par<a href="https://pixabay.com/fr/users/Goumbik-3752482/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2047314">Goumbik</a> de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=2047314">Pixabay</a>
* [prairie](static/img/prairie.jpg) : Image par<a href="https://pixabay.com/fr/users/ilyessuti-3558510/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3123271">Ilona Ilyés</a> de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3123271">Pixabay</a>
* [about.jpg](static/img/about.jpg) : Image par<a href="https://pixabay.com/fr/users/congerdesign-509903/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4036130">congerdesign</a> de <a href="https://pixabay.com/fr/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4036130">Pixabay</a>